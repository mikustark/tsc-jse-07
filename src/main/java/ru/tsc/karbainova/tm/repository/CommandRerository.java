package ru.tsc.karbainova.tm.repository;
import ru.tsc.karbainova.tm.constant.ArgumentConst;
import ru.tsc.karbainova.tm.constant.TerminalConst;
import ru.tsc.karbainova.tm.model.Command;

public class CommandRerository {
    public static Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null,"Exit program."
    ) ;
    public static Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.HELP, "Display list of terminal commands."
    );
    public static Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.VERSION, "Display program version."
    );
    public static Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.INFO, "Display info."
    );
    public static Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ABOUT, "Display developer info."
    );

    public static final Command[] COMMANDS = new Command[] {
            EXIT, HELP, VERSION, INFO, ABOUT
    };

    public static Command[] getComands() {
        return COMMANDS;
    }
}
